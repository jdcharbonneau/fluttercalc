import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Calc Shame'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String output = '0.00';

  Widget buildBtn(String value) {
    return Expanded(
        child: OutlineButton(
      padding: EdgeInsets.all(24),
      child: Text(
        value,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
      onPressed: () => _click(value),
    ));
  }

  void _click(String text) { 
    setState(() {
      if (text == "CLEAR") {
        output = "";
      } else {
        output += text;
      }
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: new Container(
          child: Column(
            children: [
              Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.symmetric(
                  vertical: 24,
                  horizontal: 12,
                ),
                child: Text(
                  output,
                  style: TextStyle(fontSize: 48, fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: Divider(),
              ),
              Row(children: [
                buildBtn('7'),
                buildBtn('8'),
                buildBtn('9'),
                buildBtn('/'),
              ]),
              Row(children: [
                buildBtn('4'),
                buildBtn('5'),
                buildBtn('6'),
                buildBtn('*'),
              ]),
              Row(children: [
                buildBtn('1'),
                buildBtn('2'),
                buildBtn('3'),
                buildBtn('-'),
              ]),
              Row(children: [
                buildBtn('.'),
                buildBtn('0'),
                buildBtn('00'),
                buildBtn('+'),
              ]),
              Row(children: [
                buildBtn('CLEAR'),
                buildBtn('='),
              ]),
            ],
          ),
        ));
  }
}
